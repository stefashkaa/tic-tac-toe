import { AppPage } from './app.po';

describe('client App', () => {
    let page: AppPage;

    beforeEach(() => {
        page = new AppPage();
    });

    it(`should display 'Tic-Tac-Toe' title`, () => {
        page.navigateTo();
        expect(page.getParagraphText()).toEqual('Tic-Tac-Toe');
    });
});
