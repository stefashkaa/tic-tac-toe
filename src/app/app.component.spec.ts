import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { routes } from './app.module';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './components/shared/material.module';
import { GameModule } from './components/game/game.module';
import { RouterModule } from '@angular/router';

describe('AppComponent', () => {

    let comp: AppComponent;
    let fixture: ComponentFixture<AppComponent>;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent
            ],
            imports: [
                BrowserModule,
                BrowserAnimationsModule,
                MaterialModule,
                RouterModule.forRoot(routes),
                GameModule
            ]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AppComponent);
        comp = fixture.componentInstance;
    });

    it('should create the comp', () => {
        expect(comp).toBeTruthy();
    });
    it(`should have as title 'Tic-Tac-Toe'`, () => {
        expect(comp.title).toEqual('Tic-Tac-Toe');
    });
    it('should render title in a tag', () => {
        fixture.detectChanges();
        const compiled = fixture.debugElement.nativeElement;
        expect(compiled.querySelector('mat-toolbar span').textContent)
            .toContain('Tic-Tac-Toe');
    });
});
